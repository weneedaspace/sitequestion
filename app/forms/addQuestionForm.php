<?php
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Email;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;

class addQuestionForm extends Form
{
	public function initialize()
	    {
	    	$this->add(
			    new Text(
			        'text_question',
			        [
			            'placeholder' => 'Введите свой вопрос',
			        ]
			    )
			);
			$this->add(
			    new Hidden(
			        'id_user'
			    )
			);

			$this->add(
			    new Check(
			        'anonymously',
			        [
			            'placeholder' => 'Введите свою фамилию',
			        ]
			    )
			);
		}

}
