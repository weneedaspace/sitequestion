<?php
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Email;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;

class SearchForm extends Form
{
	public function initialize()
	    {
	    	$this->add(
			    new Text(
			        'sname',
			        [
			            'placeholder' => 'Введите фамилию',
			        ]
			    )
			);
		}

}
