<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Email;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Forms\Element\Date;
class SettingForm extends Form
{
	public function initialize()
	    {
	    	$this->add(
			    new Text(
			        'name',
			        [
			            'placeholder' => 'Введите своё имя',
			        ]
			    )
			);

			$this->add(
			    new Text(
			        'sname',
			        [
			            'placeholder' => 'Введите свою фамилию',
			        ]
			    )
			);

			$this->add(
			    new Date(
			        'age',
			        [
			            'maxlength'   => 3,
			            'placeholder' => 'Введите свой возраст',
			        ]
			    )
			);

			$this->add(
			    new Text(
			        'city',
			        [
			            'placeholder' => 'Город'

			        ]
			    )
			);

	    }
	   

}
