<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Email;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Date;
class SignupForm extends Form
{
	public function initialize()
	    {
	    	$validation = new Validation();

			$this->add(
			    new Text(
			        'name',
			        [
			        	'placeholder' => 'имя',
			        ]
			    )
			);

			$validation->add(
				'name',
				new PresenceOf(
				[
					'message' => 'Введите своё имя',
				]
			));

			$validation->add(
				'name',
				new StringLength(
				[
					'min'            => 2,
					'messageMinimum' => "Введённое имя слишком короткое",
					'allowEmpty' => true,
				]
			));

			$this->add(
			    new Text(
			        'sname',
			        [
			            'placeholder' => 'фамилия',
			        ]
			    )
			);

			$validation->add(
				'sname',
				new PresenceOf(
				[
					'message' => 'Введите свою фамилию',
				]
			));

			$validation->add(
				'sname',
				new StringLength(
				[
					'min'            => 4,
					'messageMinimum' => "Введённая фамилию слишком короткая",
					'allowEmpty' => true,
				]
			));

			//EMAIL
			$this->add(
			    new Email(
			        'email',
			        [
			            'placeholder' => 'Адрес электронной почты',
			        ]
			    )
			);

			$validation->add(
				'email',
				new PresenceOf(
				[
					'message' => 'Введите адрес электронной почты',
				]
			));

			
			//AGE
			$this->add(
			    new Date(
			        'age'
			    )
			);

			$validation->add(
				'age',
				new PresenceOf(
				[
					'message' => 'Введите дату рождения',
				]
			));

			//PASSWORD
			$this->add(
			    new Password(
			        'password',
			        [
			            'placeholder' => 'Пароль',
			        ]
			    )
			);

			$validation->add(
				'password',
				new PresenceOf(
				[
					'message' => 'Введите будующий пароль',
				]
			));

			//CITY
			$this->add(
			    new Text(
			        'city',
			        [
			            'placeholder' => 'Пароль',
			        ]
			    )
			);

			$validation->add(
				'city',
				new PresenceOf(
				[
					'message' => 'Введите город проживания',
				]
			));

			$this->messages = $validation->validate($_POST);
	    }

}
