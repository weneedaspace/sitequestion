<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Email;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Forms\Element\Password;
class LoginForm extends Form
{
	public function initialize()
	{
    	$validation = new Validation();

		$this->add(
		    new Email(
		        'email',
		        [
		            'placeholder' => 'Введите почту',
		        ]
		    )
		);

		$validation->add(
			'email',
			new PresenceOf(
			[
				'message' => 'Введите адрес электронной почти',
			]
		));

		$this->add(
		    new Password(
		        'password',
		        [
		            'placeholder' => 'Введите пароль'

		        ]
		    )
		);


		$validation->add(
			    'password',
			    new PresenceOf(
			        [
			            'message' => 'Введите пароль',
			        ]
			    ));

		$this->messages = $validation->validate($_POST);
	}
}




