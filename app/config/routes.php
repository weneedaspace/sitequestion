<?php 
use Phalcon\Mvc\Router;

// Create the router
$router = new Router();

$router->add(
    '/search/person/',
    [
    	'controller' => 'search',
        'active' => 'index'
    ]
);

$router->add(
    'search/person/:params[a-z\-]+',
    [
    	'controller' => 'search',
        'active' => 'index'
    ]
);

$router->add(
    'search/person/:params[а-я\-]+',
    [
    	'controller' => 'search',
        'active' => 'index'
    ]
);


$router->add(
	'/question',
	[
		'controller' => 'index',
		'action' => 'question'
	]
);

$router->add(
	'/answer',
	[
		'controller' => 'index',
		'action' => 'answer'
	]
);


$router->handle();
return $router;