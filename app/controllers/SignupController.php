<?php 
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Validation;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class SignupController extends Controller
{
    public function initialize()
    {
        $this->modul_user = new Users();
        if ($this->session->has('auth')) {
            $this->response->redirect('/');
        }
    }

    public function indexAction()
    {
        $this->form = new SignupForm();
        $this->view->form = $this->form;
        if ($this->request->isPost())
        {
            if (count($this->form->messages) > 0) {
                $errors = $this->form->messages;
                $this->view->MessagesPassword = $errors->filter('password');
                $this->view->MessagesEmail = $errors->filter('email');
                $this->view->MessagesName = $errors->filter('name');
                $this->view->MessagesSname = $errors->filter('sname');
                $this->view->MessagesCity = $errors->filter('city');
                $this->view->MessagesDate = $errors->filter('age');
            }
            else
            {
                $dataPost = $this->request->getPost();
                $messages = $this->modul_user->regist($dataPost);
                $this->response->redirect('/login'); 
            }
        }
    }
}