<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Volt;
use Phalcon\Di\FactoryDefault;
use Phalcon\Di\DiInterface;
use Phalcon\Mvc\ViewBaseInterface;


class IndexController extends Controller
{
    public function initialize()
    {
        $this->model_question = new Question();
        $this->model_user = new Users();

    }

    public function indexAction()
    {
    	if ($this->session->has('auth')) {
            $this->view->pick('index/account');
            $result = $this->model_question->getQuestionById($this->session->get('auth')['id']);
            $this->view->questions = $result;
            $this->view->users =Users::find(
            [
                'conditions' => 'id ='.$this->session->get('auth')['id']
            ]);
        }
        else
        {

        }
    }

    public function questionAction()
    {
        if ($this->session->has('auth')){
            $questions = $this->model_question->getPersonQuestionNoAnswer($this->session->get('auth')['id']);
            $this->view->form = new updateQuestionForm();
            $this->view->questions = $questions;
        }
      else
        {
            $this->response->redirect('/index');
        }

    }

    public function answerAction()
    {
        if ($this->session->has('auth'))
        {
            $questions_and_destination = $this->model_question->getQuestionBySender($this->session->get('auth')['id']);
            $this->view->questions = $questions_and_destination;
        }
        else
        {
            $this->response->redirect('/index');
        }
    }

    public function updatequestionAction()
    {
        if ($this->request->isPost()) {
            $dataPost = $this->request->getPost();
            $this->model_question->updateAnswer($dataPost);
        }
        $this->response->redirect('question');
    }

    public function show404Action()
    {
        
    }
}