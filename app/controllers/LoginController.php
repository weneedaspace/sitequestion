<?php 
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Validation;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;
class LoginController extends Controller
{
    public function initialize()
    {
        $this->modul_user = new Users();
        $this->form = new LoginForm();
        $this->view->form = $this->form;
        if ($this->session->has('auth'))
        {
            $this->response->redirect('/');
        }

    }
    public function indexAction()
    {
        if ($this->request->isPost())
            {
                $this->view->pick('login/index');
                $data = $this->request->getPost();
                $this->start($data);
            }
    }

    private function _registerSession($user)
    {
        $this->session->set('auth', array(
            'id' => $user->id,
        ));
    }

    private function _dropSession()
    {
        $this->session->remove('auth'); 
    }



    public function exitAction()
    {
         if ($this->session->has('auth')) {
            $this->_dropSession();
        }
    }

    public function start($data)
    {
        $modul = new Users();
        $user = $modul->getUserByEmailPassword($data['email'], $data['password']);
        if ($this->request->isPost() && count($this->form->messages) > 0) {
            $errors = $this->form->messages;
            if (count($errors->filter('password')) > 0 ) {
                $this->view->MessagesPassword = $errors->filter('password');
            }
            if (count($errors->filter('email')) > 0 ) {
                $this->view->MessagesEmail = $errors->filter('email');
            }
            
            
        }
        else
        {
            if (!$user) {
            $this->view->message = 'Введённые данные не верны или такого пользователя не существет. Провертьте вводимую информацию.';
            $this->view->data = $data;
            $this->view->user = $user;
        }
            else
            {
                $this->_registerSession($user);
                $this->response->redirect('/');
                
            }
        }
    }
}