<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

class SettingController extends Controller
{
    
	public function initialize()
    {
        $this->modul_user = new Users();
        $this->form = new SettingForm();
        $this->view->form = $this->form;

    }
    public function indexAction()
    {
		
		if ($this->session->has('auth')) {
            $session = $this->session->get('auth');
            $user_info = $this->modul_user->getUserById($session['id']); 
            $this->view->info = $user_info;
        }
    }

    public function saveAction()
    {
        $user_info = $this->modul_user->getUserById($session['id']); 
            $this->view->info = $user_info;
    	$this->view->pick('setting/index');
    	if ($this->request->isPost()){
        		if ($this->session->has('auth')) {
                     $session = $this->session->get('auth');
                    $data = $this->request->getPost();
                    print_r($data);
                    $this->view->message = $this->modul_user->updatePersonInfo($session['id'],$data);

                }
            }
            else
            {
                $this->response->redirect('/setting');
            }
    }
}