<?php 
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Model\Query;

class SearchController extends Controller
{ 
    public function initialize()
    {
        $this->model_question= new Question();
        $this->model_user = new Users();
    }

    public function indexAction()
    {
        $form = new SearchForm();
        $this->view->form = $form;

        if ($this->request->isPost()) {
            $dataPost = $this->request->getPost();
            $this->view->users = $this->model_user->getusersLikeSname($dataPost['sname']);
        }
        else
        {
            $this->view->users = Users::find();
        
        }
    }

    public function personAction($id)
    {
        $form = new addQuestionForm();
        $this->view->form = $form;
        $result = $this->model_question->getQuestionById($id);
        $this->view->questions = $result;
        $users= Users::find(
            [
                'conditions' => 'id ='. $id
            ]);
        if ($users->count() > 0)
        {
            $this->view->users = $users;
        }
        else
        {
            $this->response->redirect('search');
        }
    }

    public function addquestionAction()
    {
        if ($this->request->isPost()) {

            $data = $this->getQuestionData($this->request->getPost());
            $this->model_question->addQuestion($data);
        }
    }

    public function getQuestionData($dataPost)
    {
        if ($this->session->has('auth'))
        {
            $session = $this->session->get('auth');
            if (isset($dataPost['anonymously']))
            {
                $dataPost['sender'] = 62;
            }
            else
            {
                $dataPost['sender'] = $session['id'];
            } 
        }
        else
        {
            $dataPost['sender'] = 62;  
        }
        
        $data = ["text_question" =>$dataPost['text_question'],
            "answer" => '',
            "sender" => $dataPost['sender'],
            "destination" => $dataPost['id_user']

        ];
        return $data;
    }
}
