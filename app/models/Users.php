<?php 
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;

class Users extends Model
{
    private $id;
    private $name;
    private $sname;
    private $password;
    private $login;
    private $email;
    private $city;
    private $age;

    public function getId(): int
    {
        return (int) $this->id;
    }

    public function getName(): string
    {
        return (string) $this->name;
    }

    public function getSname(): string
    {
        return (string) $this->sname;
    }

    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function getLogin(): string
    {
        return (string) $this->login;
    }

    public function getEmail(): string
    {
        return (string) $this->email;
    }

    public function getCity(): string
    {
        return (string) $this->city;
    }

    public function getAge(): string
    {
        return (string) $this->age;
    }

    public function setName(string $name): Users
    {
        $this->name = $name;
        
        return $this;
    }

    public function setSname(string $sname): Users
    {
        $this->sname = $sname;
        
        return $this;
    }

    public function setEmail(string $email): Users
    {
        $this->email = $email;
        
        return $this;
    }

    public function setPossword(string $password): Users
    {
        $this->password = $password;
        
        return $this;
    }

    public function setLogin(string $login): Users
    {
        $this->login = $login;
        
        return $this;
    }

    public function setAge(string $age): Users
    {
        $this->age = $age;
        
        return $this;
    }

    public function getUserByEmailPassword($email,$password)
    {
        return $this::findFirst(
            [
                "email =  :email:  AND password =  :password: ",
                'bind' => [
                    'email'    => $email,
                    'password' => $password
                ]
            ]
        );
    }

    public function getUserById($id)
    {
        return $this::findFirst($id);
    }

    public function regist($dataPost)
    {
        return $this->save(
            $dataPost,
            [
                "name",
                "sname",
                "email",
                "password",
                "age",
                "city",
            ]
        );      
    }

    public function updatePersonInfo($id,$data)
    {
        $user = $this->getUserById($id);
        $user->name = $data['name'];
        $user->sname = $data['sname'];
        $user->age = $data['age'];
        $user->city = $data['city'];
        $message = '';
        if ($user->update() === false) {
            $message = "Не удалось обновить информацию";
        }
        else
            $message = 'Отлично, данные обнавленн';
        return $message;
    }

    public function getUsersLikeSname($sname)
    {
        return $this::find(
            [
                'conditions' => "sname LIKE :sname: ",
                'bind' =>
                [
                    'sname' => $sname."%"
                ]
            ]
        );
    }
}