<?php 
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Manager;
use Phalcon\Mvc\Model\Query\Builder;
class Question extends Model
{
    private $id;
    private $text_question;
    private $answer;
    private $sender;
    private $destination;

    public function getId(): int
    {
        return (int) $this->id;
    }

    public function getText_question(): string
    {
        return (string) $this->text_question;
    }

    public function getAnswer(): string
    {
        return (string) $this->answer;
    }

    public function getSender(): int
    {
        return (string) $this->sender;
    }

    public function getDestination(): int
    {
        return (int) $this->destination;
    }

    public function setText_question(string $text_question)
    {
        $this->text_question = $text_question;
        
        return $this;
    }

    public function setAnswer(string $answer)
    {
        $this->answer = $answer;
        
        return $this;
    }

    public function setSender(int $sender)
    {
        $this->sender = $sender;
        
        return $this;
    }

    public function setDestination(int $destination)
    {
        $this->destination = $destination;
        
        return $this;
    }

    public function getPersonQuestionNoAnswer($id)
    {
            return $this
            ->modelsManager
            ->createBuilder()
            ->columns([
                'text_question',
                'Question.id'])
            ->from('Users')
            ->innerJoin(
                'Question',
                "Users.id = Question.destination")
            ->where(" Users.id = :id: and answer = ''")
            ->getQuery()
            ->execute([
                'id' => $id,
            ]);

    }

    public function getQuestionById($id)
    {

        return $this
            ->modelsManager
            ->createBuilder()
            ->columns([
                'text_question',
                'answer',
                'Question.id'])
            ->from('Users')
            ->innerJoin(
                'Question',
                "Users.id = Question.destination")
            ->where(" Users.id = :id: and answer <> ''")
            ->getQuery()
            ->execute([
                'id' => $id,
            ]);
    }

    public function addQuestion($data)
    {
    	 $this->save(
            $data,
            [
                "text_question",
                "sender",
                "destination",
                "answer"
            ]
        );

    	return $data;
    }
    
    public function updateAnswer($data)
    {
        $question = Question::findFirst($data['id_question']);
        $question->answer = $data['text_answer'];
        if ($question !== false) {
            $question->update();
        }   
    }

    public function getQuestionBySender($id)
    {
        return $this
            ->modelsManager
            ->createBuilder()
            ->columns([
                'text_question',
                'answer',
            ])
            ->from('Users')
            ->innerJoin(
                'Question',
                "Users.id = Question.sender")
            ->where("Users.id= :id: and Question.answer!='' ")
            ->getQuery()
            ->execute([
                'id' => $id,
            ]);
        }
}