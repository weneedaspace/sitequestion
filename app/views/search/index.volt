<h2>Поиск людей</h2>
{{ this.tag.form("") }}
        <label for="sname">Фамилия</label>
        {{ form.render('sname') }}
        {{ submit_button('Поиск') }}
</form>

{% if  users is iterable %}

    <table class="table table-bordered table-hover">
        <thead class="thead-light">
        <tr>
            <th>Имя</th>
            <th>Фамиля</th>
            <th>Город</th>
        </tr>
        </thead>
        <tbody>
        {% for user in users %}
            <tr>
                
                <td> {{ link_to('search\\person\\' ~ user.id, user.name, 'class' : 'btn btn-primary') }}</td>
                <td>{{ user.sname }}</td>
                <td> {{ user.city }}</td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
{% endif %}