<h2>Поиск людей</h2>
<?= $this->tag->form('') ?>
        <label for="sname">Фамилия</label>
        <?= $form->render('sname') ?>
        <?= $this->tag->submitButton(['Поиск']) ?>
</form>

<?php if ((is_array($users) || ($users) instanceof Traversable)) { ?>

    <table class="table table-bordered table-hover">
        <thead class="thead-light">
        <tr>
            <th>Имя</th>
            <th>Фамиля</th>
            <th>Город</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($users as $user) { ?>
            <tr>
                
                <td> <?= $this->tag->linkTo(['search\\person\\' . $user->id, $user->name, 'class' => 'btn btn-primary']) ?></td>
                <td><?= $user->sname ?></td>
                <td> <?= $user->city ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php } ?>