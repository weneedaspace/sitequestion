<h2>Авторизация пользавателя</h2>
    {{ form("") }}
    <div class="">
        <label for="email">Электронная почта</label>
        {{ textField(['email', 'placeholder':'ваша почта']) }}
        {% if MessagesEmail is iterable %}
             {% for mess in MessagesEmail %}
             <p class="error"> {{ mess }} </p>
             {% endfor %}
        {% endif %}
    </div>

    <div class="">
        <label for="password">Пароль</label>
        {{ passwordField(['password','placeholder':'ваш пароль']) }}
        {% if MessagesPassword is iterable %}
            {% for mess in MessagesPassword %}
                <p class="error"> {{ mess }} </p>
            {% endfor %}
        {% endif %}
    </div>
    <div>
        {{ submit_button("Авторизация") }}
    </div>
    {{ message }}
</form>