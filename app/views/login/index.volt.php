<h2>Авторизация</h2>
    <?= $this->tag->form(['']) ?>
    <div class="">
        <label for="email">Электронная почта</label>
        <?= $this->tag->textfield(['email', 'placeholder' => 'ваша почта']) ?>
        <?php if ((is_array($MessagesEmail) || ($MessagesEmail) instanceof Traversable)) { ?>
             <?php foreach ($MessagesEmail as $mess) { ?>
             <p class="error"> <?= $mess ?> </p>
             <?php } ?>
        <?php } ?>
    </div>

    <div class="">
        <label for="password">Пароль</label>
        <?= $this->tag->passwordfield(['password', 'placeholder' => 'ваш пароль']) ?>
        <?php if ((is_array($MessagesPassword) || ($MessagesPassword) instanceof Traversable)) { ?>
            <?php foreach ($MessagesPassword as $mess) { ?>
                <p class="error"> <?= $mess ?> </p>
            <?php } ?>
        <?php } ?>
    </div>
    <div>
        <?= $this->tag->submitButton(['Авторизация']) ?>
    </div>
    <?= $message ?>
</form>