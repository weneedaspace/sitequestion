<h2>Настройки личных данных</h2>

{{ form("setting/save") }}
    <div>
        <label for="name">Имя</label>
        {{ text_field('name','value' : info.name, 'placeholder': "Имя") }}
    </div>

    <div>
        <label for="sname">Фамилия</label>
        {{ text_field('sname','value' : info.sname, 'placeholder': "Фамилия") }}
    </div>

    <div>
        <label for="age">Возраст</label>
        {{ date_field('age','value': info.age) }}
    </div>

    <div>
        <label for="city">Город</label>
        {{ text_field('city','value' : info.city, 'placeholder': "город") }}
    </div>

    {{ submit_Button("Сохранить") }}
    <div>
        {% if message is defined %}
        {{ message }}
        {% endif %}
    </div>

</form>
