<h2>Настройки личных данных</h2>

<?= $this->tag->form(['setting/save']) ?>
    <div>
        <label for="name">Имя</label>
        <?= $this->tag->textField(['name', 'value' => $info->name, 'placeholder' => 'Имя']) ?>
    </div>

    <div>
        <label for="sname">Фамилия</label>
        <?= $this->tag->textField(['sname', 'value' => $info->sname, 'placeholder' => 'Фамилия']) ?>
    </div>

    <div>
        <label for="age">Возраст</label>
        <?= $this->tag->dateField(['age', 'value' => $info->age]) ?>
    </div>

    <div>
        <label for="city">Город</label>
        <?= $this->tag->textField(['city', 'value' => $info->city, 'placeholder' => 'город']) ?>
    </div>

    <?= $this->tag->submitButton('Сохранить') ?>
    <div>
        <?php if (isset($message)) { ?>
        <?= $message ?>
        <?php } ?>
    </div>

</form>
