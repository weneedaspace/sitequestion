<h2>Регистрация</h2>

<?= $this->tag->form(['']) ?>

<div>
    <label for="name">Имя</label>
    <?= $this->tag->textField(['name']) ?>
</div>

    <?php if ((is_array($MessagesName) || ($MessagesName) instanceof Traversable)) { ?>
        <?php foreach ($MessagesName as $mess) { ?>
            <p class="error"> <?= $mess ?></p>
        <?php } ?>
    <?php } ?>


    <div>
            <label for="sname">Фамилия</label>
            <?= $this->tag->textField(['sname']) ?>
    </div>

    <?php if ((is_array($MessagesSname) || ($MessagesSname) instanceof Traversable)) { ?>
        <?php foreach ($MessagesSname as $mess) { ?>
            <p class="error"> <?= $mess ?></p>
        <?php } ?>
    <?php } ?>

    <div>
        <label for="email">Электронная почта</label>
        <?= $this->tag->textField(['email']) ?>
    </div>

    <?php if ((is_array($MessagesEmail) || ($MessagesEmail) instanceof Traversable)) { ?>
        <?php foreach ($MessagesEmail as $mess) { ?>
            <p class="error"> <?= $mess ?></p>
        <?php } ?>
    <?php } ?>

    <div>
        <label for="password">Пароль</label>
        <?= $this->tag->passwordField(['password']) ?>
    </div>

    <?php if ((is_array($MessagesPassword) || ($MessagesPassword) instanceof Traversable)) { ?>
        <?php foreach ($MessagesPassword as $mess) { ?>
            <p class="error"> <?= $mess ?></p>
        <?php } ?>
    <?php } ?>

    <div>
        <label for="age">Пароль</label>
        <?= $this->tag->dateField(['age']) ?>
    </div>

    <?php if ((is_array($MessagesDate) || ($MessagesDate) instanceof Traversable)) { ?>
        <?php foreach ($MessagesDate as $mess) { ?>
            <p class="error"> <?= $mess ?></p>
        <?php } ?>
    <?php } ?>

    <p>
        <label for="city">Город</label>
        <?= $this->tag->textField(['city']) ?>
    </p>

    <?php if ((is_array($MessagesCity) || ($MessagesCity) instanceof Traversable)) { ?>
        <?php foreach ($MessagesCity as $mess) { ?>
            <p class="error"> <?= $mess ?></p>
        <?php } ?>
    <?php } ?>

    <div>
        <?= $this->tag->submitButton('Регистрация') ?>
    </div>
</form>

