<h2>Регистрация Пользователя</h2>

{{ form("") }}

<div>
    <label for="name">Имя</label>
    {{ text_field('name')}}
</div>

    {% if MessagesName is iterable %}
        {% for mess in MessagesName %}
            <p class="error"> {{ mess }}</p>
        {% endfor %}
    {% endif %}


    <div>
            <label for="sname">Фамилия</label>
            {{ text_field('sname') }}
    </div>

    {% if MessagesSname is iterable %}
        {% for mess in MessagesSname %}
            <p class="error"> {{ mess }}</p>
        {% endfor %}
    {% endif %}

    <div>
        <label for="email">Электронная почта</label>
        {{ text_field('email') }}
    </div>

    {% if MessagesEmail is iterable %}
        {% for mess in MessagesEmail %}
            <p class="error"> {{ mess }}</p>
        {% endfor %}
    {% endif %}

    <div>
        <label for="password">Пароль</label>
        {{ password_field('password')}}
    </div>

    {% if MessagesPassword is iterable %}
        {% for mess in MessagesPassword %}
            <p class="error"> {{ mess }}</p>
        {% endfor %}
    {% endif %}

    <div>
        <label for="age">Пароль</label>
        {{ date_field('age')}}
    </div>

    {% if MessagesDate is iterable %}
        {% for mess in MessagesDate %}
            <p class="error"> {{ mess }}</p>
        {% endfor %}
    {% endif %}

    <p>
        <label for="city">Город</label>
        {{ text_field('city')}}
    </p>

    {% if MessagesCity is iterable %}
        {% for mess in MessagesCity %}
            <p class="error"> {{ mess }}</p>
        {% endfor %}
    {% endif %}

    <div>
        {{ submit_Button("Регистрация") }}
    </div>
</form>

