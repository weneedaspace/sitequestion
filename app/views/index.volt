<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Phalcon Tutorial</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="\css\style.css">
</head>
<style>
	.menu ul
	{
		display: flex;
	}
	.menu ul li
	{
		margin-right: 50px;
		list-style: none;
	}
</style>
<body>
<div class="container">
    <div class="row">
    	<div class="col">
    			<div class="menu">
		<ul>
			<li><a href="/">Дом</a></li>
			<li><a href="/search">Поиск</a></li>

			{% if this.session.has('auth') %}
				<li><a href="/setting/">Личные данные</a></li>
				<li><a href="/question">Вопросы</a></li>
				<li><a href="/answer">Ответы</a></li>
				<li><a href="/login/exit">Выход</a></li>
				{% else %}
				<li><a href="/signup/">Регистрация</a></li>
				<li><a href="/login/">Авторизация</a></li>
			{% endif %}
		</ul>
	</div>
    	</div>
    </div>
    <div class="row">
    	<div class="col">
    		{{ this.getContent() }}
    	</div>
    </div>
</div>
</body>
</html>