<div class="info">
    {% if users is defined %}
    	{% for user in users %}
            <p> {{ user.name }} <p>
            <p> {{ user.sname }} <p>
            <p> {{ user.city }} <p>
            <p> {{ user.age }} <p>
        {% endfor %}
    {% endif %}

</div>
<div class="questions">
    {% if questions is defined %}
    {% for question in questions %}
            <div class="question">
                <p>Вопрос: <span> {{ question.text_question }} </span><p>
                <p>Ответ: <span> {{ question.answer }} </span><p>
            </div>
    {% endfor %}
    {% endif %}
</div>