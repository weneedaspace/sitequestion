<h2>Вопросы</h2>
<div class="questions">
    <?php if ((is_array($questions) || ($questions) instanceof Traversable)) { ?>
        <?php foreach ($questions as $question) { ?>
            <?= $this->tag->form('index/updatequestion') ?>
            <div class="question">
                <p>Вопрос: <span> <?= $question->text_question ?></span><p>
                <?= $this->tag->textField(['text_answer']) ?>
                <?= $this->tag->hiddenField(['id_question', 'value' => $question->id]) ?>
                <?= $this->tag->submitButton('Ответить') ?>
            </div>
            </form>
            <?php } ?>
        
    <?php } else { ?>
    <h5> Вам не задали новых вопросов </h5>
    <?php } ?>    
</div>