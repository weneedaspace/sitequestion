<div class="div">
	<p>Пройтите авторизацию</p>	
	<?php echo $this->tag->linkTo(["/login", 'Страница авторизации', 'class' => 'btn btn-primary']);?>
</div>

<div class="div">
	<p>Если у вас нет аккаунта, то пройтиде регистрацию</p>	
	<?php echo $this->tag->linkTo(["/signup", 'Страница регистрации', 'class' => 'btn btn-primary']);?>
</div>
