<h2>Вопросы</h2>
<div class="questions">
    {% if questions is iterable %}
        {% for question in questions %}
            {{ this.tag.form("index/updatequestion") }}
            <div class="question">
                <p>Вопрос: <span> {{ question.text_question }}</span><p>
                {{ text_field('text_answer') }}
                {{ hidden_field('id_question','value' : question.id) }}
                {{ submit_Button("Ответить") }}
            </div>
            </form>
            {% endfor %}
        
    {% else %}
    <h5> Вам не задали новых вопросов </h5>
    {% endif %}    
</div>