<?php
//Dima
use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Application;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Url as UrlProvider;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Mvc\Dispatcher as MvcDispatcher;
use Phalcon\Events\Event;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Acl\Role;
use Phalcon\Session\Adapter\Files as Session;
use Phalcon\Mvc\View\Engine\Volt;

// Определяем некоторые константы с абсолютными путями
// для использования с локальными ресурасами
define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

// Регистрируем автозагрузчик
$loader = new Loader();

$loader->registerDirs(
    [
        APP_PATH . '/controllers/',
        APP_PATH . '/models/',
        APP_PATH . '/plugins/',
        APP_PATH . '/forms/',
        APP_PATH . '/config/',
        APP_PATH . '/library/'
    ]
);

$loader->register();

// Создаём контейнер DI
$di = new FactoryDefault();
// астраиваем компонент представлений
$di->set(
    'voltService',
    function ($view, $di) {
        $volt = new Volt($view, $di);

        $volt->setOptions(
            [
                'compiledPath'      => '../app/compiled-templates/',
                'compiledExtension' => '.compiled',
            ]
        );

        return $volt;
    }
);
$di->set(
    'view',
    function () {
        $view = new View();
        $view->setViewsDir(APP_PATH . '/views/');
        $view->registerEngines(
            [
                '.volt' => 'Phalcon\Mvc\View\Engine\Volt',
            ]
        );
        return $view;
    }
);


// Setup a base URI
$di->set(
    'url',
    function () {
        $url = new UrlProvider();
        $url->setBaseUri('/');
        return $url;
    }
);

$di->set('router', function(){
    require __DIR__.'/../app/config/routes.php';
    return $router;
});

// Настраиваем сервис для работы с БД
$di->set(
    'db',
    function () {
        return new DbAdapter(
            [
                'host'     => 'localhost',
                'username' => 'root',
                'password' => '',
                'dbname'   => 'questiondb',
            ]
        );
    }
);

$di->set(
    'session',
    function () {
        $session = new Session();

        $session->start();

        return $session;
    }
);

$di->set(
    'dispatcher',
    function () {
        // Create an event manager
        $eventsManager = new EventsManager();

        // Attach a listener for type 'dispatch'
        $eventsManager->attach(
            'dispatch',
            function (Event $event, $dispatcher) {
                // ...
            }
        );

        $dispatcher = new MvcDispatcher();

        // Bind the eventsManager to the view component
        $dispatcher->setEventsManager($eventsManager);

        return $dispatcher;
    },
    true
);

    # https://stackoverflow.com/questions/24446258/phalcon-not-found-page-error-handler
    $di->set('dispatcher', function() {
        $eventsManager = new \Phalcon\Events\Manager();
    
        $eventsManager->attach("dispatch:beforeException", function($event, $dispatcher, $exception) {
    
            //Handle 404 exceptions
            if ($exception instanceof \Phalcon\Mvc\Dispatcher\Exception) {
                $dispatcher->forward(array(
                    'controller' => 'index',
                    'action' => 'show404'
                ));
                return false;
            }
    
            //Handle other exceptions
            /*$dispatcher->forward(array(
                'controller' => 'index',
                'action' => 'show503'
            ));*/
            
            return false;
        });
    
        $dispatcher = new \Phalcon\Mvc\Dispatcher();
    
        //Bind the EventsManager to the dispatcher
        $dispatcher->setEventsManager($eventsManager);
    
        return $dispatcher;
    
    }, true);


$application = new Application($di);

try {
    // Handle the request
    $response = $application->handle();

    $response->send();
} catch (\Exception $e) {
    echo 'Exception: ', $e->getMessage();
}

